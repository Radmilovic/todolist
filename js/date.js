
    let currentTime = new Date();
    console.log(currentTime);
    setTimeout(function() {
        let numberOfItems = document.querySelector('.cont_princ_lists > ul');
        const twoDaysTime = 48 * 60 * 60 * 1000;
        const twoWeeksTime = twoDaysTime * 7;

        for(var i = 1; i < numberOfItems.children.length; i++) {
            
            //vraca datume u formatu 1.1.2001.
        let dateElement = numberOfItems.childNodes[i].childNodes[2].childNodes[0].childNodes[0];

        
            //razdvajamo u array koji sadrzi [dan], [mesec], [godinu]
        let dateFromDOMSplit = dateElement.innerHTML.split(".");
        
            //sklapamo deadline tako sto u construktor Date objekta ubacujemo parametre GODINA-MESEC-DAN
        let deadline = new Date(dateFromDOMSplit[2], dateFromDOMSplit[1] - 1, dateFromDOMSplit[0]);
            
        let filterData = {};
        if(deadline.getTime() < currentTime.getTime()) {
            filterData = {
                id: i
            }
        }
        if((deadline.getTime() - currentTime.getTime())-twoDaysTime) {
            filterData = {
                id: i
            }
        }
   $("#dateFilter").change(function(){
        //console.log($("#dateFilter")[0].value);
        numberOfItems.innerHTML = " ";
        if($("#dateFilter")[0].value == "over") {
            if(deadline.getTime() < currentTime.getTime()) {
                // numberOfItems.childNodes[i].classList.add("display_none_add");
                // console.log(dateElement.parentNode.parentNode.parentNode);
                // console.log(numberOfItems.childNodes[i]);
                // dateElement.parentNode.parentNode.parentNode.classList.remove("display_none_add");
                console.log(filterData);
                $.ajax({
                    url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
                    method: 'GET',
                    data: filterData,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length > 0) {
                        for(i=0; i<data.length; i++) {
                             console.log('element ' + i, data[i]);
                            data[i].date = new Date( data[i].date );
                            createListItem( data[i] );
                        }
                    }
                }).fail();

            }
        }
        if($("#dateFilter")[0].value == "overInTwoDays") {
            if(((deadline.getTime() - currentTime.getTime()) < twoDaysTime) && (deadline.getTime() - currentTime.getTime()) > 0) {
                // numberOfItems.childNodes[i].classList.add("display_none_add");
                // console.log(dateElement.parentNode.parentNode.parentNode);
                // console.log(numberOfItems.childNodes[i]);
                // dateElement.parentNode.parentNode.parentNode.classList.remove("display_none_add");
                console.log(filterData);
                $.ajax({
                    url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
                    method: 'GET',
                    data: filterData,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length > 0) {
                        for(i=0; i<data.length; i++) {
                             console.log('element ' + i, data[i]);
                            data[i].date = new Date( data[i].date );
                            createListItem( data[i] );
                        }
                    }
                }).fail();

            }
        }

        if($("#dateFilter")[0].value == "overInTwoWeeks") {
            if(((deadline.getTime() - currentTime.getTime()) < twoWeeksTime) && (deadline.getTime() - currentTime.getTime()) > twoDaysTime) {
                // numberOfItems.childNodes[i].classList.add("display_none_add");
                // console.log(dateElement.parentNode.parentNode.parentNode);
                // console.log(numberOfItems.childNodes[i]);
                // dateElement.parentNode.parentNode.parentNode.classList.remove("display_none_add");
                console.log(filterData);
                $.ajax({
                    url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
                    method: 'GET',
                    data: filterData,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length > 0) {
                        for(i=0; i<data.length; i++) {
                             console.log('element ' + i, data[i]);
                            data[i].date = new Date( data[i].date );
                            createListItem( data[i] );
                        }
                    }
                }).fail();

            }
        }
        
        
    });
        
        }
    }, 1000);
    
