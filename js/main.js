$(document).ready(function() {
    $('#deadline').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
    });

    $('#actionFilter').change(function(){

        let filter = {}
        if($(this).val() !== '') {
            filter = {
                action: $(this).val()
            }
        }

        // Clear data from html DOM
        const ul = document.querySelector('.cont_princ_lists > ul');
        ul.innerHTML = '';

        // Load already saved data - also filtered
        $.ajax({
            url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
            method: 'GET',
            data: filter,
            dataType: 'json'
        }).done(function(data) {
            if (data.length > 0) {
                for(i=0; i<data.length; i++) {
                    // console.log('element ' + i, data[i]);
                    data[i].date = new Date( data[i].date );
                    createListItem( data[i] );
                }
            }
        }).fail();
    });




    // Load already saved data
    $.ajax({
        url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
        method: 'GET',
        dataType: 'json'
    }).done(function(data) {
        if (data.length > 0) {
            for(i=0; i<data.length; i++) {
                // console.log('element ' + i, data[i]);
                data[i].date = new Date( data[i].date );
                createListItem( data[i] );
            }
        }

    }).fail();

   
});

function add_new() {
    document.getElementsByClassName("cont_crear_new")[0].classList.toggle("cont_crear_new_active");
}




function add_to_list() {
    
    let deadline = new Date(document.getElementById('deadline').value);


    let newTodoItem = {
        actionValue : document.getElementById("action_select").value,
        titleValue : document.getElementsByClassName("input_title_desc")[0].value,
        dateValue : deadline,
        descValue : document.getElementsByClassName("input_description")[0].value,
        deleteTask : function() {
            let confirmQuestion = confirm("Are you sure that you want to delete this task?");
            if(confirmQuestion) {
                parent[0].removeChild(newTodo);
            }
        }
    };

    createListItem({
        action: newTodoItem.actionValue,
        title: newTodoItem.titleValue,
        date: newTodoItem.dateValue,
        desc: newTodoItem.descValue,
    });

    //checking if the form is filled, if not - you can't add new task
    if(!isFilled()) {
        return;
    }


    function isFilled() {
        if(newTodoItem.titleValue == '' || newTodoItem.descValue == '' ) {
            document.getElementsByClassName('alert')[0].innerHTML = "Please fill in Title and Description!";
            return false;
        }else{
            document.getElementsByClassName('alert')[0].innerHTML = "";
            return true;
        }
    }
}

function createListItem(newTodoItem) {
    
    // we saw that <ul> exists, so this is the first item to be created and later filled with content
    let newTodo = document.createElement("li"); 
    newTodo.classList.add("list_shopping");
    newTodo.classList.add("list_" + newTodoItem.action.toLowerCase());
    newTodo.classList.add("li_num_0_1"); 

    // creating first div which contains action value, should be added to <li> parent later
    let div1 = document.createElement("div"); 
    div1.classList.add("col_md_1_list");
    div1.innerHTML = '<p>' + newTodoItem.action + '</p>'; 

    // creating second div which contains title and descrition, should be added to <li> parent later
    let div2 = document.createElement('div');
    div2.classList.add("col_md_2_list");
    let div2Title = '<h4>' + newTodoItem.title + '</h4>';
    let div2Desc = '<p>' + newTodoItem.desc + '</p>'; 
    div2.innerHTML = div2Title + div2Desc; 
    div2.children[1].classList.toggle('display_none_add');
    //by clicking on title we show and hide description
    div2.children[0].addEventListener('click', function()  {
        div2.children[1].classList.toggle('display_none_add');
    });

    // third div contains date, should be added to <li> parent later
    let div3 = document.createElement('div');
    div3.classList.add('col_md_3_list');
    div3.innerHTML = '<div class="cont_text_date"><p>' + newTodoItem.date.toLocaleDateString() + '</p></div>' + '<div class="cont_btns_options"><ul><li><a href="#" onclick="finish_action();"><i class="material-icons"></i></a></li></ul></div>';
    
    //delete button should delete it's own todoitem.
    const deleteBtn = document.createElement('button');
    deleteBtn.innerHTML = '<i class="material-icons">&#xE872;</i>'; 
    deleteBtn.style.color="#772020";
    deleteBtn.style.position = 'relative';
    deleteBtn.style.top = "18px";
    deleteBtn.style.right = "62px";
    deleteBtn.addEventListener('click', deleteTask); 

    //appending children to the <li> parent.
    newTodo.appendChild(div1);
    newTodo.appendChild(div2);
    newTodo.appendChild(div3);
    newTodo.appendChild(deleteBtn); 

    let parent = document.getElementsByTagName("ul"); // everything is added to this precreated HTML item.
    parent[0].appendChild(newTodo); //finally adding <li> to ul
    
    // Restarting value after every new item added to the list
    document.getElementById('action_select').value = 'SHOPPING';
    document.querySelector('.input_title_desc').value = '';
    //document.getElementById('date_select').value = 'TODAY';
    document.querySelector('.input_description').value = '';

    
}
function deleteTask() {
    let confirmQuestion = confirm("Are you sure that you want to delete this task?");
    if(confirmQuestion) {
        let parent = document.getElementsByTagName("ul");
        parent[0].removeChild(document.getElementsByTagName("li")[0]);
    }
}

function finish_action() {
    var x = event.target;
    let parentNode = x.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
    parentNode.classList.toggle("list_finish_state");
}

